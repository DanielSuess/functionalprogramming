(println "Willkommen zum Wikinger-Game")
(defn swap [i]
  (case i
    1 3
    2 2
    3 1))

(defn nimm [x]
  "Nimm 1-3. Letzter verliert" ; Zeile fuer Doku, Pre und Post Conditions
  (println "vor dir sind " x " Steine")
  (println "Wieviele (1-3) Steine nimmst du?") (flush) ; flush um die terminal pipe zu füllen

  ;let: gefrieren /zuweisung einer final
  (let [you-take (read-string (read-line))
        i-take (swap you-take)]
    (println "Wir haben jetzt " (- x you-take) " items")
    (if (zero? (- x you-take))
      (println "Du hast verloren!")
      (do
        (println "Ich nehme " i-take " Objekte weg!")
        ; rekursiver Aufruf
        (nimm (- x you-take i-take))))))

; starte spiel
(nimm (inc (* 4 (rand-int 7))))