/**
 * Game of Live in functional programming style.
 * Prepared for using different rule sets.
 * Includes 3 world set ups for glider, flic flac and r-Pentomino
 *
 * Running (snapshot) instance may be found at https://repl.it/B7tW/4
 *
 * Inspired from http://codingdojo.org/cgi-bin/index.pl?KataGameOfLife
 */

var ALIVE = "x",
    DEAD = "_";

/**
 * Simulation of Game of Live. Will be printed to console.
 * @param worldAsList
 * @param handleLivingRulesFn
 * @param totalNumberOfRuns
 */
function gameOfLive( worldAsList, handleLivingRulesFn, totalNumberOfRuns ) {

    // entry point
    handleRun( convertStringListTo2DArray( worldAsList ) );

    //---------
    // nested functions
    //---------

    function handleRun( worldArray, numberOfRunsDone ) {
        if ( !numberOfRunsDone ) {
            console.log( "Starting with:" );
            printArray( worldArray );
            numberOfRunsDone = 0;
        }

        // save updated world for multiple upcoming usages
        // is this ok for functional programming, otherwise: how to?
        var updatedWorld = worldArray.map( function ( rowValue, rowIndex ) {
            return rowValue.map( function ( columnValue, columnIndex ) {
                //console.log( "[" + rowIndex + "," + columnIndex + "] = " + numberOfNeighbors + " status: " + columnValue + " --> " + handleLivingResult );
                // live update the world
                //worldArray[ rowIndex ][ columnIndex ] = handleLivingRulesFn( columnValue, countNeighbors( worldArray, rowIndex, columnIndex ) );
                // do not live update, use snapshot for evolution
                return handleLivingRulesFn( columnValue, countNeighbors( worldArray, rowIndex, columnIndex ) );
            } )
        } );

        // increase runs
        numberOfRunsDone++;

        console.log( "" );
        console.log( "Run " + numberOfRunsDone );
        printArray( updatedWorld );

        if ( numberOfRunsDone != totalNumberOfRuns ) {
            // recursive call
            handleRun( updatedWorld, numberOfRunsDone );
        }
    }

    function countNeighbors( worldArray, posX, posY ) {
        var counter = 0; // is counter variable in functional programming ok?

        // using 'undefined' evaluates to false for 'index out of bounds' handling
        // above
        worldArray[ posX - 1 ] && worldArray[ posX - 1 ][ posY - 1 ] == ALIVE ? counter++ : null;
        worldArray[ posX ] && worldArray[ posX ][ posY - 1 ] == ALIVE ? counter++ : null;
        worldArray[ posX + 1 ] && worldArray[ posX + 1 ][ posY - 1 ] == ALIVE ? counter++ : null;
        // aside
        worldArray[ posX - 1 ] && worldArray[ posX - 1 ][ posY ] == ALIVE ? counter++ : null;
        worldArray[ posX + 1 ] && worldArray[ posX + 1 ][ posY ] == ALIVE ? counter++ : null;
        // below
        worldArray[ posX - 1 ] && worldArray[ posX - 1 ][ posY + 1 ] == ALIVE ? counter++ : null;
        worldArray[ posX ] && worldArray[ posX ][ posY + 1 ] == ALIVE ? counter++ : null;
        worldArray[ posX + 1 ] && worldArray[ posX + 1 ][ posY + 1 ] == ALIVE ? counter++ : null;

        return counter;
    }

    function convertStringListTo2DArray( inputList ) {
        return inputList.map( function ( value, index ) {
            return value.split( "" ); // each char one entry
        } );
    }

    function printArray( array ) {
        array.map( function ( valueRowArray, index ) {
                console.log( valueRowArray.join( '' ) );
            }
        )
    }
}

/**
 * Original Rule Set:
 *  1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
 *  2. Any live cell with more than three live neighbours dies, as if by overcrowding.
 *  3. Any live cell with two or three live neighbours lives on to the next generation.
 *  4. Any dead cell with exactly three live neighbours becomes a live cell.
 * @param cellStatus
 * @param numberOfNeighbors
 * @return {*}
 */
function originalGOLRulesFn( cellStatus, numberOfNeighbors ) {
    if ( cellStatus === ALIVE ) {
        switch ( numberOfNeighbors ) {
            // keep living
            case 2:
            case 3:
                return ALIVE;
            // 0,1: died due loneliness; 4+: died due overpopulation
            default:
                return DEAD;
        }
    } else {
        if ( numberOfNeighbors === 3 ) {
            //
            return ALIVE;
        } else {
            return DEAD;
        }
    }
}


/**
 * A Glider moving downwards
 * @type {string[]}
 */
var inputListGlider = [
    "__x__________",
    "___x_________",
    "_xxx_________",
    "_____________",
    "_____________",
    "_____________",
    "_____________",
    "_____________"
];

var inputListFlicFlac = [
    "______xx_____",
    "______x_x____",
    "_____________",
    "________x_x__",
    "_________xx__",
    "___x_________",
    "___x_________",
    "___x_________"
];

var inputListRPentomino = [
    "_____________",
    "_____________",
    "_____xx______",
    "____xx_______",
    "_____x_______",
    "_____________",
    "_____________",
    "_____________"
];

var numberOfRoundsToGo = 10;

gameOfLive( inputListGlider, originalGOLRulesFn, numberOfRoundsToGo );