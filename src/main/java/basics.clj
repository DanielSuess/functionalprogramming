; single line comment: all behind semicolon is comment
; multi line comment: use function 'comment'
(comment
  "Multi line comment, incl. upcomming code as comment"
  (println "Hi -  i wont be executed")
  )

; fimple funtion call of print line
(println "hello world") ; print hello world

; method names in front, every function can handle multiple arguments (like args[])
(println (+ 1 2 3) " demo addition") ; prints 6 demo addition

; after single quote the rest is not evaluated, just printed?
(println '(+ 1 2 3) " demo string") ; prints (+ 1 2 3) demo string

; function with head, documentation and body
(defn HelloPow [name val] ; function head
  "Liefert Name und Quadrat zurueck" ; doc
  (str "Hi ", name, "! ... Quadrat von ",val, " ist ", (* val val)))

; call of the function
(println (HelloPow "du da" 5)) ; prints Hi du da! ... Quadrat von 5 ist 25

; better documentation
'(doc *Pow)

; finals
(def greeting "Hallo final, wie geht's?")
(println greeting)

(println (+ 1 2 3)) ; => 6
(println (/ 2.0 4)) ; => 0.5
(println (< 1 2 3)) ; true
(print '(< 1 9 3))
(println (< 1 9 3)) ; false
(print '(pos? (+ 1 9 -5)))
(println (pos? (+ 1 9 -5))) ; 1 + 9 - 5 = 4 > 0